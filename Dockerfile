# Use an official Python runtime as a parent image
FROM alpine:latest

# Set the working directory in the container
WORKDIR /app


# Install Poetry
ENV POETRY_VERSION=1.6.1
RUN pip install poetry==$POETRY_VERSION

# Copy only the dependency files to the container
COPY pyproject.toml poetry.lock /app/

# Install dependencies using Poetry
RUN poetry install

# Copy the rest of the application files into the container
COPY . /app/

# Expose the port on which your FastAPI app runs
EXPOSE 8000

# Command to run the application using Uvicorn
CMD ["poetry", "run", "uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]

