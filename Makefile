docker-test:
	./docker-run-test.sh

test:
	echo "Running tests..."

docker-build-push:
	echo "Running docker build and push..."

integration-test:
	echo "Running integration tests..."

update-new-ml-model:
	echo "Pushing model to model repository..."

deploy-to-eks:
	echo "Deploying to EKS..."